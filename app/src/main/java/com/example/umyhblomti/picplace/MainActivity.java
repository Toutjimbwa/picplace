package com.example.umyhblomti.picplace;

import android.graphics.drawable.Drawable;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import android.support.design.widget.FloatingActionButton;

import java.util.ArrayList;
import android.widget.Button;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;

public class MainActivity extends AppCompatActivity
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient googleApiClient;private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private Uri fileUri;
    Button button;

    PicArrayAdapter adapter;
    ArrayList<Drawable> drawables;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawables = new ArrayList<>();
        Drawable cats = getResources().getDrawable(R.drawable.cats_placeholder);
        drawables.add(cats);

        adapter = new PicArrayAdapter(this, R.layout.picplace_list_item, drawables);
        ListView listView = (ListView)findViewById(R.id.MAINACTIVITY_LISTVIEW);
        listView.setAdapter(adapter);

        FloatingActionButton FAB = (FloatingActionButton)findViewById(R.id.MAINACTIVITY_BUTTON_PHOTO);
        FAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeToast("FAB pressed");
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                // start the image capture Intent
                startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        });

        // Create an instance of GoogleAPIClient.
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

    }

    private void makeToast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data){

        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // Image captured and saved to fileUri specified in the Intent
                Toast.makeText(this, "Image saved to:\n" +
                        data.getData(), Toast.LENGTH_LONG).show();
                Uri uri = data.getData();
                Log.i("Tag", "data.getData:" + uri);
                String filePath = uri.toString();
                try {
                    GeoTagger.geoTagWithCurrentLocation(this,filePath);
                    Picture pic =
                            new Picture(GeoTagger.getLatitude(filePath),GeoTagger.getLongitude(filePath),GeoTagger.getAltitude(filePath),filePath);
                    Log.i("TAG","Latitude: " + pic.getLatitude());
                    Log.i("TAG","Longitude: " + pic.getLongitude());
                    Log.i("TAG","Altitude: " + pic.getAltitude());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ///Skapa ny

            } else if (resultCode == RESULT_CANCELED) {
                // User cancelled the image capture
            } else {
                // Image capture failed, advise user
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void onStart() {
        googleApiClient.connect();
        super.onStart();
    }

    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    public GoogleApiClient getGoogleApiClient() {
        return googleApiClient;
    }
}
