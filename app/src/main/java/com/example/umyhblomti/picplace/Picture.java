package com.example.umyhblomti.picplace;

import android.graphics.drawable.Drawable;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by umyhlarsle on 2016-04-05.
 */
public class Picture {

    float latitude;
    float longitude;
    double altitude;
    String filepath;

    public Picture(float latitude, float longitude, double altitude, String filepath){
        this.latitude=latitude;
        this.longitude=longitude;
        this.altitude=altitude;
        this.filepath=filepath;

    }

    public float getLatitude() {
        return latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public String getFilepath() {

        return filepath;
    }
    public static Drawable getDrawableFromUrl(URL url) {
        try {
            InputStream is = url.openStream();
            Drawable d = Drawable.createFromStream(is, "src");
            return d;
        } catch (MalformedURLException e) {
            // e.printStackTrace();
        } catch (IOException e) {
            // e.printStackTrace();
        }
        return null;
    }
}
