package com.example.umyhblomti.picplace;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by umyhblomti on 2016-04-04.
 */
public class PicArrayAdapter extends ArrayAdapter {

    private Context mContext;
    private List mObjects;
    private int mResource;

    public PicArrayAdapter(Context context, int resource, List objects) {
        super(context, resource, objects);
        mContext = context;
        mObjects = objects;
        mResource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v;
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(mResource, parent, false);
        }else{
            v = convertView;
        }

        ImageView imageView = (ImageView)v.findViewById(R.id.LISTITEM_IMAGEVIEW);
        Drawable drawable = (Drawable)mObjects.get(position);//TODO get drawable using a function on the picture object instead of just getting a drawable
        imageView.setImageDrawable(drawable);

        TextView textView = (TextView)v.findViewById(R.id.LISTITEM_COORDINATES);
        String coordinates_N = "0.0000";//TODO get coordinates from mObjects somehow
        String coordinates_W = "0.0000";
        String text = "N: "+coordinates_N+"\n"+"W: "+coordinates_W;
        textView.setText(text);

        return v;
    }
}
