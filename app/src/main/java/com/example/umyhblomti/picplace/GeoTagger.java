package com.example.umyhblomti.picplace;

import android.Manifest;
import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.media.ExifInterface;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.location.LocationServices;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by umyhpuscdi on 2016-04-04.
 */
public class GeoTagger {

    private static StringBuilder sb = new StringBuilder(20);

    public static void geoTagWithCurrentLocation(MainActivity mainActivity, String filePath)
            throws IOException {

        ExifInterface exifInterface = new ExifInterface(filePath);
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            if (mainActivity.checkSelfPermission(
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && mainActivity.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                return;
            }
        }
        Location lastLocation =
                LocationServices.FusedLocationApi.getLastLocation(mainActivity.getGoogleApiClient());
        if (lastLocation != null) {
            exifInterface.setAttribute(ExifInterface.TAG_GPS_LONGITUDE,
                    convert(lastLocation.getLongitude()));
            exifInterface.setAttribute(ExifInterface.TAG_GPS_LATITUDE,
                    convert(lastLocation.getLatitude()));
            exifInterface.setAttribute(ExifInterface.TAG_GPS_ALTITUDE,
                    convert(lastLocation.getAltitude()));
        }
    }

    public static float getLatitude(String filePath) throws IOException {
        ExifInterface exifInterface = new ExifInterface(filePath);
        float[] latLong = new float[2];
        exifInterface.getLatLong(latLong);
        return latLong[0];
    }

    public static float getLongitude(String filePath) throws IOException {
        ExifInterface exifInterface = new ExifInterface(filePath);
        float[] latLong = new float[2];
        exifInterface.getLatLong(latLong);
        return latLong[1];
    }

    public static double getAltitude(String filePath) throws IOException {
        ExifInterface exifInterface = new ExifInterface(filePath);
        return exifInterface.getAltitude(0);
    }

    private static String convert(double latitude) {
        latitude = Math.abs(latitude);
        final int degree = (int)latitude;
        latitude *= 60;
        latitude -= degree * 60.0d;
        final int minute = (int)latitude;
        latitude *= 60;
        latitude -= minute * 60.0d;
        final int second = (int)(latitude * 1000.0d);
        sb.setLength(0);
        sb.append(degree);
        sb.append("/1,");
        sb.append(minute);
        sb.append("/1,");
        sb.append(second);
        sb.append("/1000,");
        return sb.toString();
    }
}
